import re
import os

files =  os.listdir('.')
for f in files:
    if f[:14] == 'ap_phys_c_mech' and f[-4:] == '.tex':
        year = f[15:19]
        problem = f[21]
        title = 'AP Physics C Mechanics %s, Problem %s' % (year, problem)
        print title
        latex_file = open('tmp.tex', mode='w')
        latex_file.write(r"""\documentclass[11pt]{exam}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{units}
\usepackage{array}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{fancyhdr}
\renewcommand{\baselinestretch}{1}
\newcolumntype{K}[1]{>{\centering\arraybackslash}p{#1}}
\renewcommand{\arraystretch}{1.2}
\graphicspath{{./images/}}
\pagestyle{fancy}
"""
        )
        latex_file.write(r"""
        \lhead{\textsc{"""+title+r"""}}
        \chead{}
        \rhead{Page \thepage}
        \lfoot{}
        \cfoot{}
        \rfoot{}
""")
        latex_file.write(r"""
\begin{document}
\begin{questions}
\include{"""+f[:-4]+"""}
\end{questions}
\end{document}
"""
        )
        latex_file.close()
        os.system('pdflatex -interaction=batchmode tmp.tex')
        os.system('pdflatex -interaction=batchmode tmp.tex')
        os.system('mv tmp.pdf '+f[:-4]+'.pdf')
        os.system('rm tmp.*')
os.system('rm *.aux')
