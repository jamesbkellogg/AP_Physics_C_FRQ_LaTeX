import os

years = range(1976,1999)
#years = [1998]
for year in years:
    wordfile = 'C'+str(year)+'.docx'
    latexfile = 'ap_phys_c_mech_'+str(year)+'.tex'
    os.system('pandoc %s --extract-media=media -o %s'  % (wordfile, latexfile))
    #move media files
    for image_file in os.listdir('./media/media'):
        num = int(image_file[5:-4])
        newfile = 'ap_phys_c_mech_%s-%03g.png'%(year, num)
        os.system('mv media/media/%s images/%s' % (image_file, newfile))
        os.system("sed -i -e 's:media/media/%s:images/%s:g' %s" %(image_file, newfile, latexfile))
    #break into individual problem files
    problem = 0
    if_echo = False
    with open(latexfile) as f:
        for line in f:
            if line[:6] == '%gM%g'%(year, problem+1) or line[:6] == '%gE%g'%(year, problem+1):
                line = line[7:]
                problem += 1
                if problem > 1:
                    outfile.close()
                if problem <4:
                    outfile = open('ap_phys_c_mech_%s_q%g.tex' % (year, problem), mode='w')
                    outfile.write(r"""\question \
""")
                    if_echo = True
                else:
                    if_echo = False
                    break
            if if_echo:
                if line.startswith('\includegraphics'):
                    outfile.write(r"""\begin{center}
"""+line+"""
\end{center}
""")
                else:
                    outfile.write(line)
    os.system('rm ' + newfile)
os.system('rm -rf media')
os.system('rm *.tex-e')
