\question \
\begin{center}
\includegraphics[width=0.8\textwidth]{ap09-002}
\end{center}
A block of mass $M/2$ rests on a frictionless horizontal table, as shown above. It is connected to one end of a
string that passes over a massless pulley and has another block of mass $M/2$ hanging from its other end. The
apparatus is released from rest.
\begin{parts}
\part
Derive an expression for the speed $v_h$ of the hanging block as a
function of the distance $d$ it descends.\\[1in]
\uplevel{Now the block and pulley system is replaced by a uniform rope of length L and mass $M$, with one end of the
rope hanging slightly over the edge of the frictionless table. The rope is released from rest, and at some time later
there is a length $y$ of rope hanging over the edge, as shown below. 
\begin{center}
\includegraphics[width=0.8\textwidth]{ap09-003}
\end{center}
Express your answers to parts (b), (c), and (d) in terms of $y$, $L$, $M$, and fundamental constants.}
\part
Determine an expression for the force of gravity on the hanging part
of the rope as a function of $y$.\\[1in]
\part
Derive an expression for the work done by gravity on the rope as a function of $y$, assuming $y$ is initially zero.\\[.5in]
\part
Use the work-energy theorem :w  to derive an expression for the speed $v_r$ of the rope as a function of
$y$.\\[1in]
\part
 of work to derive an expression for the speed $v_r$ of the rope as a function of
$y$.\\[1in]


\part
The hanging block and the right end of the rope are each allowed to fall a distance $L$ (the length of the rope).
The string is long enough that the sliding block does not hit the pulley. Indicate whether $v_h$ from part (a) or
$v_r$ from part (d) is greater after the block and the end of the rope have traveled this distance.\\
\begin{center}
\underline{\hspace{3em}} $v_h$ is greater.\hspace{2em}
\underline{\hspace{3em}} $v_r$ is greater.\hspace{2em}
\underline{\hspace{3em}} The speeds are equal.\\
\end{center}
\bigskip
Justify your answer.\\[.5in]
\end{parts}
