import re
import os

#ap_test = 'ap_phys_c_mech_'
ap_test = 'ap_phys_1_'

root='ap15_frq_physics_1'
#year = root[:4]
year = '2015'
figure_root = ap_test+year
os.system('pdftotext -f 2 ' + root + '.pdf ' + root + '.txt')
os.system('pdfimages -f 2 -png ' + root + '.pdf '+figure_root)
os.system('mv *.png images/.')


#open files
text_file_name = root + '.txt'
root = ap_test + year
text_file = open(text_file_name, mode='r')

skip_lines = ['College Board', 'NEXT PAGE', 'FREE-RESPONSE', 'STOP', 'END OF EXAM', 'collegeboard']

replacements = {
    'acceleration a':'acceleration $a$',
    'velocity v':'velocity $v$',
    'position x':'position $x$',
    'mass m':'mass $m$',
    'mass M':'mass $M$',
    'time t':'time $t$',
    'length d':'length $d$',
    'distance d':'distance $d$',
    'distance D':'distance $D$',
    'distance x':'distance $x$',
    '____':'\underline{\hspace{3em}}',
    '_____':'\underline{\hspace{3em}}'
}

def close_document(depth, question, latex_file):
    if depth == 2:
        latex_file.write('\\end{subparts}\n')
        depth = 1
    if depth == 1:
        latex_file.write('\\end{parts}\n')
        depth = 0
    latex_file.close()

#set up flag to skip header material
question = 0
depth = 0

#latex_file.write('\\begin{questions}\n')

for line in text_file:
    line = line.strip()
    #look for start of problem
    if re.search('Mech.* *[1-9]', line) is not None:
        question += 1
        line = line[8:]

        #manage output files
        if question > 1:
            close_document(depth, question, latex_file)
        latex_file_name = root + '_q' + str(question) + '.tex'
        latex_file = open(latex_file_name, mode='w')
        print latex_file_name

        depth = 0

        #start new question
        latex_file.write('\\question \\ \n')
        latex_file.write('\\begin{center} \n')
        text ='\\includegraphics[width=0.8\\textwidth]{'+figure_root+'-000} \n'
        latex_file.write(text)
        latex_file.write('\\end{center} \n')

    if question > 0:
        # open a new subsection
        if re.match('\(a\)*', line.strip()) is not None:
            if depth == 2:
                latex_file.write('\\end{subparts} \n')
            depth = 1
            latex_file.write('\\begin{parts}\n')
        elif re.match('i\.', line.strip()) is not None:
            depth = 2
            latex_file.write('\\begin{subparts}\n')
        # write a new part to this subsection
        if re.match('\([a-z]\)', line.strip()) is not None:
            if depth == 2:
                latex_file.write('\\end{subparts} \n')
                depth = 1
            latex_file.write('\part\n')
            line = line.strip()[3:]+'\n'
        elif re.match('i+\.', line.strip()) is not None:
            latex_file.write('\subpart\n')
            line = line.lstrip('i').lstrip('.')+'\n'

        #look for triggers not to print the line
        skip = False
        for item in skip_lines:
            if item in line:
                skip = True
        if re.search('\-[0-9]+\-', line) is not None:
            skip = True
        if not skip:
            for replacement in replacements:
                line = line.replace(replacement, replacements[replacement])
            if len(line.strip()) > 0:
                latex_file.write(line.strip()+'\n')



close_document(depth, question, latex_file)
text_file.close()
